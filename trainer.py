import tensorflow as tf
import numpy as np
import os
import dataset

# np.random.seed(1)
# tf.set_random_seed(2)

train_path = "training_data"
classes = os.listdir(train_path)
validation_perc = 0.2
batch_size = 16
img_size = 128
channels = 3

data = dataset.readTrainingData(train_path, img_size, classes, validation_perc)

session = tf.Session()
x = tf.placeholder(tf.float32, shape=[None, img_size, img_size, channels],
                   name='x')
y_true = tf.placeholder(tf.float32, shape=[None, len(classes)], name='y_true')
y_true_cls = tf.argmax(y_true, axis=1)


def genWeights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))


def genBiases(size):
    return tf.Variable(tf.constant(0.05, shape=[size]))


def createConvLayer(input, input_channels, filter_size, num_filters):
    weights = genWeights(shape=[filter_size, filter_size, input_channels, num_filters])
    biases = genBiases(num_filters)
    layer = tf.nn.conv2d(input=input, filter=weights,
                         strides=[1, 1, 1, 1], padding='SAME')
    layer += biases
    layer = tf.nn.max_pool(value=layer, ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1], padding='SAME')
    layer = tf.nn.relu(layer)

    return layer


def createFlattenLayer(layer):
    shape = layer.get_shape()
    num = shape[1:4].num_elements()
    layer = tf.reshape(layer, [-1, num])
    return layer


def createFullyConnLayer(input, num_inputs, num_outputs, use_relu=True):
    weights = genWeights(shape=[num_inputs, num_outputs])
    biases = genBiases(num_outputs)
    layer = tf.matmul(input, weights) + biases
    if use_relu:
        layer = tf.nn.relu(layer)
    return layer


filter_size_conv1 = 3
num_filters_conv1 = 32

filter_size_conv2 = 3
num_filters_conv2 = 32

filter_size_conv3 = 3
num_filters_conv3 = 64

fc_layer_size = 128

layer_conv1 = createConvLayer(input=x,
                              input_channels=channels,
                              filter_size=filter_size_conv1,
                              num_filters=num_filters_conv1)
layer_conv2 = createConvLayer(input=layer_conv1,
                              input_channels=num_filters_conv1,
                              filter_size=filter_size_conv2,
                              num_filters=num_filters_conv2)

layer_conv3 = createConvLayer(input=layer_conv2,
                              input_channels=num_filters_conv2,
                              filter_size=filter_size_conv3,
                              num_filters=num_filters_conv3)

layer_flat = createFlattenLayer(layer_conv3)

layer_fc1 = createFullyConnLayer(input=layer_flat,
                                 num_inputs=layer_flat.get_shape()[1:4].num_elements(),
                                 num_outputs=fc_layer_size,
                                 use_relu=True)

layer_fc2 = createFullyConnLayer(input=layer_fc1,
                                 num_inputs=fc_layer_size,
                                 num_outputs=len(classes),
                                 use_relu=False)

y_pred = tf.nn.softmax(layer_fc2, name='y_pred')
y_pred_cls = tf.argmax(y_pred, axis=1)
session.run(tf.global_variables_initializer())

# optymalizacja, kosztem jest "różnica" wyniku obliczonego od dobrego
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits=layer_fc2,
                                                           labels=y_true)
cost = tf.reduce_mean(cross_entropy)
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)

# dokładność - to co się wyświetli
correct_prediction = tf.equal(y_pred_cls, y_true_cls)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

session.run(tf.global_variables_initializer())

total_iterations = 0
saver = tf.train.Saver()


def train(num_iteration):
    global total_iterations

    for i in range(total_iterations, total_iterations + num_iteration):
        x_batch, y_true_batch, _ = data.train.next_batch(batch_size)
        x_val_batch, y_val_batch, _ = data.valid.next_batch(batch_size)

        feed_dict_train = {x: x_batch, y_true: y_true_batch}
        feed_dict_val = {x: x_val_batch, y_true: y_val_batch}

        session.run(optimizer, feed_dict=feed_dict_train)

        if i % int(data.train._setSize / batch_size) == 0:
            val_loss = session.run(cost, feed_dict=feed_dict_val)
            epoch = int(i / int(data.train._setSize/batch_size))

            acc = session.run(accuracy, feed_dict=feed_dict_train)
            val_acc = session.run(accuracy, feed_dict=feed_dict_val)
            msg = "Epoch {0:<8d} Training accuracy: {1:.2%}, Validation accuracy: {2:.2%},  Validation loss (cost): {3:.3f}"
            print(msg.format(epoch + 1, acc, val_acc, val_loss))
            saver.save(session, './model/dogs-cats-model')

    total_iterations += num_iteration


train(num_iteration=300)
