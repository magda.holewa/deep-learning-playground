import numpy as np
import skimage.io
import skimage.transform
import os
import glob


class DataSet:
    def __init__(self, images, labels, classes):
        self._images = images
        self._labels = labels
        self._classes = classes
        self._setSize = images.shape[0]
        self._epochsDone = 0
        self._indexInEpoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def classes(self):
        return self._classes

    @property
    def setSize(self):
        return self._setSize

    @property
    def epochsDone(self):
        return self._epochsDone

    @property
    def indexInEpoch(self):
        return self._indexInEpoch

    def next_batch(self, batch_size):
        start = self._indexInEpoch
        self._indexInEpoch += batch_size
        if self._indexInEpoch > self._setSize:
            start = 0
            self._indexInEpoch = batch_size
            self._epochsDone += 1
            assert batch_size <= self._setSize
        end = self._indexInEpoch
        return self._images[start:end], self._labels[start:end], self._classes[start:end]


class DataCollection:
    pass


def readTrainingData(dirpath, img_size, classes, validation_perc):
    data = DataCollection()
    # print("start")
    images = []
    labels = []
    # tmpimg = None
    cl = []
    for type in classes:
        index = classes.index(type)
        path = os.path.join(dirpath, type, '*g')
        files = glob.glob(path)
        for file in files:
            pic = skimage.io.imread(file)
            pic = skimage.transform.resize(pic, (img_size, img_size))
            pic = skimage.img_as_float32(pic)
            images.append(pic)
            # tmpimg = pic
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            cl.append(type)
            print("file loaded: " + file)
    # print(tmpimg)
    images = np.array(images)
    labels = np.array(labels)
    cl = np.array(cl)

    """załadowane, teraz wrzucamy do obiektu"""
    perm = np.random.permutation(len(images))
    images = images[perm]
    labels = labels[perm]
    cl = cl[perm]
    valid_size = int(validation_perc * len(images))

    data.train = DataSet(images[:valid_size], labels[:valid_size],
                         cl[:valid_size])
    data.valid = DataSet(images[valid_size:], labels[valid_size:],
                         cl[valid_size:])

    return data


# readTrainingData("training_data", 28, ['cats', 'dogs'], 0.9)
