import tensorflow as tf
import numpy as np
import skimage.io
import skimage.transform
import os

classes = os.listdir('training_data')
img_size = 128
channels = 3

session = tf.Session()
saver = tf.train.import_meta_graph('./model/dogs-cats-model.meta')
saver.restore(session, tf.train.latest_checkpoint('./model/'))
graph = tf.get_default_graph()


def loadFile(filepath):
    pic = skimage.io.imread(filepath)
    pic = skimage.transform.resize(pic, (img_size, img_size))
    pic = skimage.img_as_float32(pic)
    return pic


def recognize(filepath):
    x_batch = []
    x_batch.append(loadFile(filepath))
    y_test = np.zeros((1, len(classes)))

    y_pred = graph.get_tensor_by_name("y_pred:0")
    x = graph.get_tensor_by_name("x:0")
    y_true = graph.get_tensor_by_name("y_true:0")

    feed_dict_test = {x: x_batch, y_true: y_test}
    result = session.run(y_pred, feed_dict=feed_dict_test)

    return result


path = input("File path: ")

results = recognize(path)[0]
print(list(zip(classes, results)))
